<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

class UserController extends Controller
{
    //
    function register(Request $req)
    {
        $user = new User;

        $body = json_decode($req->getContent());

        $user->name = $body->name;
        $user->email = $body->email;
        $user->password = Hash::make($body->password);

        $user->save();

        return $user;
    }
}
