//import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import Header from './components/Header';

import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import AddProduct from './pages/AddProduct'
import UpdateProduct from './pages/UpdateProduct'

function App() {
  return (
    <div className="App">
        <BrowserRouter>
            <Header />
            <h1>Laravel Dashboard</h1>
            <Routes>
                <Route path='/' element={<Home/>} />
                <Route path='/login' element={<Login/>} />
                <Route path='/register' element={<Register/>} />
                <Route path='/add-product' element={<AddProduct/>} />
                <Route path='/update-product' element={<UpdateProduct/>} />
            </Routes>
        </BrowserRouter>
    </div>
  );
}

export default App;
