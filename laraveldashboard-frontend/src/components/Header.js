import { Navbar, Container, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import styles from './Header.module.css';

function Header() {
    return (
        <Navbar bg="dark" variant="dark" expand="xxl">
            <Container>
                <Navbar.Brand><Link to='/' className={`${styles.home_link}`}>Laravel Dashboard</Link></Navbar.Brand>
                <Nav className={`me-auto ${styles.navbar_wrapper}`}>
                    <Link to="/login">Login</Link>
                    <Link to="/register">Register</Link>
                    <Link to="/add-product">Add Prodact</Link>
                    <Link to="/update-product">Update Prodact</Link>
                </Nav>
            </Container>
        </Navbar>
    )
}

export default Header