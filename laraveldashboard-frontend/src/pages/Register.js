import './Register.css';

import React, {useState} from 'react';
import { useNavigate } from "react-router-dom";

import {Container, Row, Col} from "react-bootstrap"
import {Button} from "react-bootstrap"

function Register() {
    
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    
    let navigate = useNavigate()

    async function signUp() {
        let item = {
            'name': name,
            'email': email,
            'password': password
        }

        let res = await fetch("http://localhost:8000/api/register", {
            method: "POST",
            body: JSON.stringify(item)
        })

        let resBody = await res.json();
        localStorage.setItem('user-info', JSON.stringify(resBody))
        navigate('/add')
    }

    return (
        <div>
            <h1>Register Page</h1>
            <Container>
                <Row className="register-row">
                    <Col>
                        <input type="text" className="form-control" placeholder="name" value={name} onChange={(e)=> setName(e.target.value)} />
                    </Col>
                </Row>
                <Row className="register-row">
                    <Col>
                        <input type="text" className="form-control" placeholder="email" value={email} onChange={(e)=> setEmail(e.target.value)} />
                    </Col>
                </Row>
                <Row className="register-row">
                    <Col>
                        <input type="password" className="form-control" placeholder="" value={password} onChange={(e)=> setPassword(e.target.value)} />
                    </Col>
                </Row>
                <Row className="register-row">
                    <Col>
                        <Button onClick={signUp} >Register</Button>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Register